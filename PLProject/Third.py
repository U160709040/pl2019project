import heapq
import sys
file_one = open(sys.argv[1], "r")
file_second = open(sys.argv[2], "r")


def split_lines(file):
    for line in file:
        output = line.split()
        return output


text_input = split_lines(file_one)
text2_input = split_lines(file_second)

# using heapify to convert list into heap
heapq.heapify(text_input)
heapq.heapify(text2_input)

# printing created heap
print("The created heap is : ", end="")
print(list(text_input))

print("The second created heap is : ", end="")
print(list(text2_input))


def check(t1, t2):
    arr = []
    for string in t1:
        if string not in t2:
            arr.append(string)

    print(arr)


print("First minus second ")
check(text_input, text2_input)


print("Second minus first ")
check(text2_input, text_input)

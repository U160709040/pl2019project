import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Second {


        // Driver code
        public static void main(String args[])
        {
            // Creating empty priority queue
            PriorityQueue<String> heap_one = new PriorityQueue<String>();
            PriorityQueue<String> heap_two = new PriorityQueue<String>();
            File file = new File(args[0]);
            File file2 = new File(args[1]);
            //File file = new File("C:\\Users\\bk\\IdeaProjects\\PL_Project_Java\\src\\input.txt");
            //File file2 = new File("C:\\Users\\bk\\IdeaProjects\\PL_Project_Java\\src\\input2.txt");
            // Adding items to the pQueue using add()
           get_input(file, heap_one);
           get_input(file2, heap_two);


            // Printing all elements
            System.out.println("The first elements of the file:");
            Iterator itr = heap_one.iterator();
            while (itr.hasNext())
                System.out.print(itr.next() + "  ");
            System.out.println();
            System.out.println("The first elements of the second file:");
            Iterator itr2 = heap_two.iterator();
            while (itr2.hasNext())
                System.out.print(itr2.next()  + "  ");
            System.out.println();
            System.out.print("######## FILE1 minus FILE2:   ");  check(heap_one, heap_two);
            System.out.println();
            System.out.print("######## FILE2 minus FILE1:  ");  check(heap_two, heap_one);



        }

    public static void get_input(File f, PriorityQueue m) {
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(f));
            String read = null;
            while ((read = in.readLine()) != null) {
                String[] splited = read.split("\\s+");
                for (String part : splited) {
                    m.add(part);
                }
            }
        } catch (IOException e) {
            System.out.println("There was a problem: " + e);
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (Exception e) {
                System.out.println("There was a problem: " + e);

            }
        }

    }
    public  static void check(PriorityQueue m1, PriorityQueue m2) {
        for(Object object : m1) {
            if (!(m2.contains(object))) {
                System.out.print(object + " ");
        }


        }

    }

}

